# This Project Is A Lie

## Table of Contents
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Port Forwarding](#port-forwarding)
- [Configuration](#configuration)
<br />


## Prerequisites
- Powershell 5.1 or higher
    - _**Note:** Windows 10 / Windows Server 2016 and higher have Powershell 5.1 built in_
    - For the newest release of Powershell [**click here**](https://learn.microsoft.com/en-us/powershell/scripting/install/installing-powershell/view=powershell-7.4)
- Python 3.9 or higher
- MongoDB
<br />


## Installation
### <ins>**Step 1**</ins>
- [ ] Clone the Repository
```pwsh
git clone https://gitlab.com/sela-tracks/1099/students/serbeepsalot/finalproject.git
```


### <ins>**Step 2**</ins>
- [ ] Run the quickload Powershell script
```pwsh
.\quickload.ps1
```
- [ ] Wait for the process to complete
<br />


## Port Forwarding
- [ ] To port forward Jenkins and ArgoCD Run the PortForward Powershell script
```pwsh
.\PortForward.ps1
```
- [ ] Once the script is loaded it will provide you with the passwords for Jenkins and ArgoCD, Use a browser of your choice go to: 
    - [ ] **Jenkins:** https://localhost:8080
    - [ ] **ArgoCD:** http://localhost:8081
<br />


## Configuration
