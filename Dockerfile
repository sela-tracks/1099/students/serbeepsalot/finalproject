FROM python:3.9-slim

WORKDIR /flask-app

COPY requirements.txt Main.py /flask-app/
RUN pip install --upgrade pip && pip install -r requirements.txt

COPY . .

# EXPOSE 5000

CMD ["python", "Main.py"]
