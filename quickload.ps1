
# Install Helm
helm repo add stable https://charts.helm.sh/stable
helm repo update

# Create namespaces
kubectl create namespace jenkins
kubectl create namespace argocd
kubectl create namespace observation
kubectl create namespace database

# Deploy Jenkins with plugins
$jenkinsValues = @"
controller:
  installPlugins:
    - kubernetes:3937.vd7b_82db_e347b_
    - workflow-aggregator:596.v8c21c963d92d
    - git:5.2.0
    - configuration-as-code:1647.ve39ca_b_829b_42
    - gitlab-plugin:1.7.14
    - blueocean:1.27.4
    - workflow-multibranch:756.v891d88f2cd46
    - login-theme:46.v36f624efb_23d
    - prometheus:2.2.3
    - github-oauth:588.vf696a_350572a_
"@
Set-Content -Path ./jenkins-values.yaml -Value $jenkinsValues
helm install jenkins stable/jenkins -f jenkins-values.yaml --namespace jenkins

# Deploy ArgoCD
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
helm install argocd argo/argo-cd --namespace argocd

# # Deploy Grafana & Prometheus
# helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
# helm repo add grafana https://grafana.github.io/helm-charts
# helm repo update

# helm install prometheus prometheus-community/prometheus --namespace observation
# helm install grafana grafana/grafana --namespace observation

# # Deploy MongoDB
# helm repo add bitnami https://charts.bitnami.com/bitnami
# helm repo update
# helm install mongodb bitnami/mongodb --namespace database

# # Create Ingress
# $ingressConfig = @"
# apiVersion: networking.k8s.io/v1
# kind: Ingress
# metadata:
#   name: cluster-ingress
#   namespace: default
# spec:
#   rules:
#   - host: localhost
#     http:
#       paths:
#       - path: /ci
#         pathType: Prefix
#         backend:
#           service:
#             name: jenkins
#             port:
#               number: 8080
#       - path: /cd
#         pathType: Prefix
#         backend:
#           service:
#             name: argocd-server
#             port:
#               number: 80
#       - path: /monitor
#         pathType: Prefix
#         backend:
#           service:
#             name: prometheus-server
#             port:
#               number: 80
#       - path: /observe
#         pathType: Prefix
#         backend:
#           service:
#             name: grafana
#             port:
#               number: 80
#       - path: /database
#         pathType: Prefix
#         backend:
#           service:
#             name: mongodb
#             port:
#               number: 27017
# "@
# Set-Content -Path ./ingress.yaml -Value $ingressConfig
# kubectl apply -f ingress.yaml

# # Apply mongoDO configuration
# $mongoPath = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
# Set-Location $mongoPath
# kubectl apply -f .\PersistentVolumeClaim.yaml
# kubectl apply -f .\ConfigMap.yaml
# kubectl apply -f .\StatefulSet.yaml
# kubectl apply -f .\Service.yaml
