# Write-Host "Checking for required modules"
# try {

# } catch {

# }

# Converts the encoded base64 string to a readable string
function DecodeBase64String([string]$base64String) {
    $decodedBytes = [System.Convert]::FromBase64String($base64String)
    $decodedString = [System.Text.Encoding]::UTF8.GetString($decodedBytes)
    return $decodedString
}


# Define namespaces and labels for each service
$services = @{
    'jenkins' = @{
        Namespace = 'jenkins'
        Pod_Name = 'svc/jenkins'
        Port = '8080:8080'
    }
    'argocd' = @{
        Namespace = 'argocd'
        Pod_Name = 'svc/argocd-server'
        Port = '8081:443'
    }
    # 'grafana' = @{
    #     Namespace = 'observation'
    #     Pod_Name = 'svc/grafana'
    #     Port = '3000:80'
    # }
    # 'prometheus' = @{
    #     Namespace = 'observation'
    #     Pod_Name = 'svc/prometheus-server'
    #     Port = '9090:80'
    # }
    # 'mongodb' = @{
    #     Namespace = 'database'
    #     Pod_Name = 'svc/mongodb'
    #     Port = '27017:27017'
    # }
}

# Retrieve All admin passwords
$base64JenkinsPassword = kubectl get secret --namespace jenkins jenkins -o jsonpath="{.data.jenkins-admin-password}"
$base64ArgoCDPassword = kubectl get secret --namespace argocd argocd-initial-admin-secret -o jsonpath="{.data.password}"
$base64GrafanaPassword = kubectl get secret --namespace observation grafana -o jsonpath="{.data.admin-password}"

if ($services.jenkins) {
    $services.jenkins.Password = DecodeBase64String $base64JenkinsPassword
}
if ($services.jenkins) {
    $services.argocd.Password = DecodeBase64String $base64ArgoCDPassword
}
if ($services.jenkins) {
    $services.grafana.Password = DecodeBase64String $base64GrafanaPassword
}

# Loop through each service and start port-forwarding in a background job
$PaddingLong = 25
$PaddingShort = 15
Write-Host "VVV  Initializing Port-Forwarding  VVV`n"

foreach ($serviceKey in $services.Keys) {
    $service = $services[$serviceKey]
    $Namespace = $service.Namespace
    $Pod_Name = $service.Pod_Name
    $Port = $service.Port

    # Displays the pod's characteristics (and password if one exists)
    $message = "Pod - " + $Pod_Name.PadRight($PaddingLong) + "Namespace - " + $Namespace.PadRight($PaddingShort) + "Port - " + $Port.PadRight($PaddingShort)
    if ($service.Password) {
        $message += "Password - $($service.Password)"
    } 
    Write-Host "$message"


    Start-Job -ScriptBlock {
        param($Namespace ,$Pod_Name ,$Port ,$Proxy)
        kubectl port-forward $($Pod_Name) -n $($Namespace) $($Port)
    } -ArgumentList $service.Namespace ,$service.Pod_Name ,$service.Port | Out-Null
}

Read-Host "`n>> To Stop Port Forwarding Press Enter <<"
Write-Host "Stopping Port Forwarding (This can take a few minutes)"
Get-Job | Stop-Job
Get-Job | Remove-Job
Write-Host "Port Forwarding Stopped"
Read-Host "Press Enter To Exit..."

exit